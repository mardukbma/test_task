<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 20.01.19
 * Time: 21:56
 */

namespace app\models;


abstract class ApiBinanceAbstract
{
    /**
     * @return mixed
     */
    abstract protected function getBalance();

}
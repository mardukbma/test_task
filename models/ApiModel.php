<?php
/**
 * Created by PhpStorm.
 * User: marduk
 * Date: 20.01.19
 * Time: 21:55
 */

namespace app\models;

use Binance\API;


class ApiModel extends ApiBinanceAbstract
{
    const API_KEY = 'IYA5q5ZHH0Tq31SnkKfJdyEuixVHsEnBqKcpZhoMlQOHHSxcJpSz2cYEoK8ZhPpX';
    const API_SECRET = 'XpY6RjYxxvQ0kR0wCs3m1pyyzciFTe4lXx2rpaTGt33XQZCcYtRGH3QbAZd9XP7N';

    public $api;

    /**
     * ApiModel constructor.
     * @throws \Exception
     */
    public function __construct()
    {
        $this->api = new API(self::API_KEY, self::API_SECRET);
        $this->api->useServerTime();
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function getBalance()
    {
        $ticker = $this->api->prices();
        $all_coin_balance = $this->api->balances($ticker);

        return $this->getPositive($all_coin_balance);
    }

    /**
     * @param array $all_coin_balance
     * @return array
     */
    private function getPositive(array $all_coin_balance = [])
    {
        $result = [];
        foreach ($all_coin_balance AS $k => $v) {
            if ($v['available'] > 0) {
                $result[$k] = $v;
            }
        }
        return $result;
    }
}
<?php

namespace app\controllers;

use app\models\ApiModel;
use Yii;
use yii\web\Controller;

class SiteController extends Controller
{
    /**
     * @return string
     * @throws \Exception
     */
    public function actionIndex()
    {
        $api = new ApiModel();
        return $this->render('index', ['balances' => $api->getBalance()]);
    }
}

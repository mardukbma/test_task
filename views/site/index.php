<?php

/* @var $this yii\web\View */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Тестовое задание';
?>
<div class="row">
    <div class="col-md-offset-4 col-md-2 head_rates">
        Валюта
    </div>
    <div class="col-md-4">
        Остаток
    </div>
</div>
<?php $form = ActiveForm::begin() ?>
<?php foreach ($balances AS $coin => $val) { ?>
    <div class="row rates mt-2">
        <div class="col-md-offset-4 col-md-2">
            <?= $coin ?>
        </div>
        <div class="col-md-4">
            <?= ArrayHelper::getValue($val, 'available') ?>
        </div>
    </div>

<?php } ?>
<div class="row">
    <div class="col-md-offset-4 col-md-2 mt-2">
        <?= Html::submitButton('Обновить', ['class' => 'btn btn-primary mt-5']) ?>
    </div>
</div>
<?php ActiveForm::end() ?>
